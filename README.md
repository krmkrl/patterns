# patterns

Design patterns from Head First

1. Clone repo - git clone https://krmkrl@bitbucket.org/krmkrl/patterns.git
2. Install IDEA (intellij)
3. Install graphviz (for plantuml) - sudo apt install graphviz
4. Import project (gradle) into IDEA
5. Open any .puml file and install IDEA plugin when prompted
