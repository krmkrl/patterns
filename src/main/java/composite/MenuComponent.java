package composite;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface MenuComponent extends Iterable<MenuComponent> {

    //Common methods
    String getName();
    String getDescription();
    void print();

    //Operation methods (leaf methods)
    default double getPrice() {
        throw new UnsupportedOperationException();
    }
    default boolean isVegetarian() {
        throw new UnsupportedOperationException();
    }

    //Composite methods (node methods)
    default void add(MenuComponent menuComponent) {
        throw new UnsupportedOperationException();
    }
    default void remove(MenuComponent menuComponent) {
        throw new UnsupportedOperationException();
    }
    default MenuComponent getChild(int pos) {
        throw new UnsupportedOperationException();
    }
    default int getNumChildren() {
        return 0;
    }

    // stream iteration
    default Stream<MenuComponent> stream() {
        return StreamSupport.stream(spliterator(), false);
    }
}
