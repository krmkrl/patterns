package composite.menus;

import composite.Menu;
import composite.MenuComponent;
import composite.MenuItem;

public class MenuStorage {

    private final MenuComponent allMenus;

    public MenuStorage() {
        allMenus = new Menu("All menus", "All menus in one big leaflet");
        allMenus.add(createBreakfastMenu());
        allMenus.add(createLunchMenu());
        allMenus.add(createDinerMenu());
    }

    public MenuComponent getAllMenus() {
        return allMenus;
    }

    private MenuComponent createBreakfastMenu() {
        MenuComponent breakfastMenu = new Menu("Breakfast menu", "Breakfast menu available from 07:00 to 11:00");
        breakfastMenu.add(new MenuItem("Egg and Bacon", "Egg and Bacon on a hot plate with lots of grease", false, 2.99));
        breakfastMenu.add(new MenuItem("Cereals", "Home made cereals made from local corn", true, 2.49));
        breakfastMenu.add(new MenuItem("Ham and Cheese sandwich", "Ham and Cheese sandwich with salad and tomato", false, 1.99));
        return breakfastMenu;
    }

    private MenuComponent createLunchMenu() {
        MenuComponent lunchMenu = new Menu("Lunch menu", "Lunch menu available from 11:00 to 15:00");
        lunchMenu.add(new MenuItem("Chicken pie", "Chicken pie with vegetables", false, 3.49));
        lunchMenu.add(new MenuItem("Vegetable soup", "Soup made from fresh vegetables, served with bread", true, 2.99));
        lunchMenu.add(new MenuItem("Pancakes", "Pancakes with lots of cream and jam, suitable for kids!", true, 2.49));
        return lunchMenu;
    }

    private MenuComponent createDinerMenu() {
        MenuComponent starterMenu = new Menu("Starter menu", "Something to start a nice dinner with");
        starterMenu.add(new MenuItem("Garlic bread", "Home made bread with a ton of garlic", true, 0.99));
        starterMenu.add(new MenuItem("Cheese plate", "Various local cheese", true, 1.49));
        starterMenu.add(new MenuItem("Serrano roll", "A delicious roll with serrano and cream cheese", false, 1.99));

        MenuComponent mainCourseMenu = new Menu("Main courses", "Cheap or fancy, we have it both!");
        mainCourseMenu.add(new MenuItem("Seafood pizza", "Sourdough pizza baked in wooden oven with fresh condiments from the sea", false, 3.99));
        mainCourseMenu.add(new MenuItem("Whale tongue", "Fried whale tongue on a plate of beats and nuts", false, 5.99));
        mainCourseMenu.add(new MenuItem("Stuffed peppers", "Peppers stuffed with oats and vegetables", true, 3.19));

        MenuComponent dessertMenu = new Menu("Desserts", "Something sweet is always nice");
        dessertMenu.add(new MenuItem("Ice cream", "Home made vanilla ice cream with fresh strawberries", true, 1.99));
        dessertMenu.add(new MenuItem("Chocolate cake", "Delicious cake with lots of tasty dark chocolate, served with cream", true, 2.49));

        MenuComponent dinerMenu = new Menu("Diner menu", "Diner menu available from 15:00 to late");
        dinerMenu.add(starterMenu);
        dinerMenu.add(mainCourseMenu);
        dinerMenu.add(dessertMenu);
        return dinerMenu;
    }
}
