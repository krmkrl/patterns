package composite.iterators;

import composite.MenuComponent;

import java.util.Iterator;
import java.util.Stack;

/*
 * DFS iteration of the MenuComponent tree
 */
public class MenuComponentIterator implements Iterator<MenuComponent> {

    private final Stack<MenuComponent> stack = new Stack<>();

    public MenuComponentIterator(MenuComponent root) {
        stack.push(root);
    }

    @Override
    public boolean hasNext() {
        return !stack.isEmpty();
    }

    @Override
    public MenuComponent next() {
        if (!hasNext()) {
            return null;
        }
        MenuComponent top = stack.pop();
        // Add children from last to first since they will be reversed again on the stack
        for (int i = top.getNumChildren() - 1; i >= 0; i--) {
            stack.push(top.getChild(i));
        }
        return top;
    }
}
