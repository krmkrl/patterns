package composite.iterators;

import composite.MenuComponent;

import java.util.Iterator;

public class EmptyIterator implements Iterator<MenuComponent> {
    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public MenuComponent next() {
        return null;
    }
}
