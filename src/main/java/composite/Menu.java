package composite;

import composite.iterators.MenuComponentIterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Menu implements MenuComponent {

    private List<MenuComponent> menuComponents = new ArrayList<>();
    private final String name;
    private final String description;

    public Menu(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void print() {
        System.out.println(toString());
        for (MenuComponent component : menuComponents) {
            component.print();
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n");
        builder.append(name);
        builder.append(", ");
        builder.append(description);
        builder.append("\n--------------------------------------------");
        return builder.toString();
    }

    @Override
    public void add(MenuComponent menuComponent) {
        menuComponents.add(menuComponent);
    }

    @Override
    public void remove(MenuComponent menuComponent) {
        menuComponents.remove(menuComponent);
    }

    @Override
    public MenuComponent getChild(int pos) {
        return menuComponents.get(pos);
    }

    @Override
    public int getNumChildren() {
        return menuComponents.size();
    }

    @Override
    public Iterator<MenuComponent> iterator() {
        return new MenuComponentIterator(this);
    }
}
