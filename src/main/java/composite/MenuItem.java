package composite;

import composite.iterators.EmptyIterator;

import java.util.Iterator;

public class MenuItem implements MenuComponent {

    private final String name;
    private final String description;
    private final boolean isVegetarian;
    private final double price;

    public MenuItem(String name, String description, boolean isVegetarian, double price) {
        this.name = name;
        this.description = description;
        this.isVegetarian = isVegetarian;
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean isVegetarian() {
        return isVegetarian;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void print() {
        System.out.println(toString());
    }

    @Override
    public Iterator<MenuComponent> iterator() {
        return new EmptyIterator();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("   ");
        builder.append(name);
        if (isVegetarian) {
            builder.append("(v)");
        }
        builder.append(", ");
        builder.append(price);
        builder.append("\n      -- ");
        builder.append(description);
        return builder.toString();
    }
}
