package composite;

import composite.menus.MenuStorage;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Waitress {
    private final MenuComponent allMenus;

    public Waitress(MenuComponent allMenus) {
        this.allMenus = allMenus;
    }

    public void printAllMenus() {
        allMenus.print();
    }

    public void printMenuWithForEach() {
        allMenus.forEach(System.out::println);
    }

    public void printVegetarianDishes() {
        System.out.println("\n Vegetarian dishes");
        System.out.println("--------------------------------------------");
        allMenus.stream()
                .filter(predicate(MenuComponent::isVegetarian))
                .forEach(MenuComponent::print);
    }

    private void printNumDishesByMenu() {
        System.out.println("\nNum dishes by menu");
        Map<String, Integer> menuToNumDishes = allMenus.stream()
                .filter((c) -> c.getNumChildren() > 0)
                .collect(Collectors.groupingBy(MenuComponent::getName, Collectors.summingInt(MenuComponent::getNumChildren)));
        System.out.println(menuToNumDishes);
    }

    private void printDishesByPrice() {
        System.out.println("\nDishes by price");
        List<String> dishesByPrice = allMenus.stream()
                .filter(predicate((c) -> c.getPrice() >= 0.0))
                .sorted(Comparator.comparing(MenuComponent::getPrice))
                .map((c) -> c.getName() + " " + c.getPrice())
                .collect(Collectors.toList());
        System.out.println(dishesByPrice);
    }

    private void printDessertMenu() {
        MenuComponent dessertMenu = getMenuComponent("Desserts").get();
        dessertMenu.forEach(System.out::println);
    }

    private Optional<MenuComponent> getMenuComponent(String name) {
        return allMenus.stream()
                .filter((c) -> c.getName().equals(name))
                .findFirst();
    }

    // predicate wrapper
    private Predicate<MenuComponent> predicate(Predicate<MenuComponent> predicate) {
        return (c) -> {
            try {
                return predicate.test(c);
            } catch (UnsupportedOperationException e) {
                return false;
            }
        };
    }

    public static void main(String[] args) {
        MenuStorage menuStorage = new MenuStorage();
        Waitress waitress = new Waitress(menuStorage.getAllMenus());
        waitress.printAllMenus();
        waitress.printMenuWithForEach();
        waitress.printVegetarianDishes();
        waitress.printNumDishesByMenu();
        waitress.printDishesByPrice();
        waitress.printDessertMenu();
    }
}
