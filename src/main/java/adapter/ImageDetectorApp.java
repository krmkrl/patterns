package adapter;

import adapter.javadetector.JavaImageDetectorImpl;
import adapter.nativedetector.ImageType;
import adapter.nativedetector.NativeImageDetector;
import adapter.nativedetector.NativeImageDetectorImpl;

public class ImageDetectorApp {

    private final NativeImageDetector imageDetector;

    public ImageDetectorApp(NativeImageDetector imageDetector) {
        this.imageDetector = imageDetector;
    }

    public void runImageDetection(String base64Image) {
        ImageType imageType = imageDetector.detectImage(base64Image);
        System.out.println("Image is: " + imageType.name());
    }

    public static void main(String[] args) {
        String base64Image = "QSBodW1hbiBvciBhbiBhbmltYWw/IEhlbGwsIEkgcmVhbGx5IGRvbid0IGtub3cuLi4gSSBzaG91bGQgYmUgYmV0dGVyIGF0IHRoaXM=";

        NativeImageDetector nativeImageDetector = new NativeImageDetectorImpl();
        ImageDetectorApp imageDetectorApp = new ImageDetectorApp(nativeImageDetector);
        imageDetectorApp.runImageDetection(base64Image);

        // and now with the new library through the adapter

        JavaImageDetectorAdapter detectorAdapter = new JavaImageDetectorAdapter(new JavaImageDetectorImpl());
        imageDetectorApp = new ImageDetectorApp(detectorAdapter);
        imageDetectorApp.runImageDetection(base64Image);
    }
}
