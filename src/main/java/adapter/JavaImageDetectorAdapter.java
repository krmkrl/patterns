package adapter;

import adapter.javadetector.JavaImageDetector;
import adapter.nativedetector.ImageType;
import adapter.nativedetector.NativeImageDetector;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;

public class JavaImageDetectorAdapter implements NativeImageDetector {

    private final JavaImageDetector javaImageDetector;

    public JavaImageDetectorAdapter(JavaImageDetector javaImageDetector) {
        this.javaImageDetector = javaImageDetector;
    }

    @Override
    public ImageType detectImage(String base64Image) {
        Image image = decodeImage(base64Image);
        adapter.javadetector.ImageType imageType = javaImageDetector.detect(image);
        return convertImageType(imageType);
    }

    private Image decodeImage(String base64Image) {
        try {
            byte[] imageBytes = Base64.getDecoder().decode(base64Image);
            return ImageIO.read(new ByteArrayInputStream(imageBytes));
        } catch (IOException ioe) {
            throw new RuntimeException("Error decoding image string");
        }
    }

    private ImageType convertImageType(adapter.javadetector.ImageType imageType) {
        if (imageType == adapter.javadetector.ImageType.HUMAN) {
            return ImageType.HUMAN;
        } else if (imageType == adapter.javadetector.ImageType.ANIMAL) {
            return ImageType.ANIMAL;
        } else {
            return ImageType.ANIMAL;
            // or throw RuntimeException if that is more appropriate
        }
    }
}
