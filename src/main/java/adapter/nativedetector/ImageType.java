package adapter.nativedetector;

public enum ImageType {
    HUMAN, ANIMAL
}
