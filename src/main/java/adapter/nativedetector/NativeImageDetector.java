package adapter.nativedetector;

public interface NativeImageDetector {
    public ImageType detectImage(String base64Image);
}

