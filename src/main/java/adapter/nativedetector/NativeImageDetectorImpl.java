package adapter.nativedetector;

import java.util.Random;

public class NativeImageDetectorImpl implements NativeImageDetector {
    @Override
    public ImageType detectImage(String base64Image) {
        // Should run image detection algorithm with pre-trained neural network
        // ... but we don't have time for that.

        Random rand = new Random();
        if (rand.nextBoolean()) {
            return ImageType.HUMAN;
        } else {
            return ImageType.ANIMAL;
        }
    }
}
