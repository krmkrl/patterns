package adapter.javadetector;

public enum ImageType {
    ANIMAL, HUMAN, ALIEN
}
