package adapter.javadetector;

import java.awt.*;

public interface JavaImageDetector {
    public ImageType detect(Image image);
}

