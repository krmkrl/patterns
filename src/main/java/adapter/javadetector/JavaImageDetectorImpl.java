package adapter.javadetector;

import java.awt.*;
import java.util.Random;

public class JavaImageDetectorImpl implements JavaImageDetector {
    @Override
    public ImageType detect(Image image) {
        // Should run image detection algorithm with pre-trained neural network
        // ... but we don't have time for that.

        Random random = new Random();
        int rand = random.nextInt(3);

        if (rand == 0) {
            return ImageType.HUMAN;
        } else if (rand == 1) {
            return ImageType.ANIMAL;
        } else {
            return ImageType.ALIEN;
        }
    }
}
