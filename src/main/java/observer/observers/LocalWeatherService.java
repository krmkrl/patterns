package observer.observers;

import observer.Observer;
import observer.observable.WeatherData;

public class LocalWeatherService implements Observer<WeatherData> {
    @Override
    public void update(WeatherData data) {
        System.out.println("Updating local weather service with new data: " + data);
    }
}
