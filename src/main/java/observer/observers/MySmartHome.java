package observer.observers;

import observer.Observer;
import observer.observable.WeatherData;

public class MySmartHome implements Observer<WeatherData> {
    @Override
    public void update(WeatherData data) {
        System.out.println("Updating my smart home with new data: " + data);
    }
}
