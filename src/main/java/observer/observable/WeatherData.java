package observer.observable;

public class WeatherData {
    private final int windSpeed;
    private final int temperature;

    public WeatherData(int windSpeed, int temperature) {
        this.windSpeed = windSpeed;
        this.temperature = temperature;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public int getTemperature() {
        return temperature;
    }

    @Override
    public String toString() {
        return "WeatherData{" +
                "windSpeed=" + windSpeed + " ms" +
                ", temperature=" + temperature + " celsius" +
                '}';
    }
}
