package observer.observable;

import observer.Observable;
import observer.Observer;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class WeatherStation implements Observable<WeatherData> {

    private final List<Observer<WeatherData>> observers = new CopyOnWriteArrayList<>();
    private WeatherData data;

    public WeatherStation(WeatherData data) {
        this.data = data;
    }

    @Override
    public void addObserver(Observer<WeatherData> observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer<WeatherData> observer) {
        observers.remove(observer);
    }

    public void update(WeatherData data) {
        this.data = data;
        notifyObservers();
    }

    private void notifyObservers() {
        observers.forEach(observer -> observer.update(data));
    }
}
