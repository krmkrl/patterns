package observer;

import observer.observable.WeatherData;
import observer.observable.WeatherStation;
import observer.observers.LocalWeatherService;
import observer.observers.MySmartHome;

public class Main {

    public static void main(String[] args) {
        WeatherStation weatherStation = new WeatherStation(new WeatherData(10, 30));

        MySmartHome homeAutomation = new MySmartHome();
        LocalWeatherService localWeatherService = new LocalWeatherService();

        weatherStation.addObserver(localWeatherService);

        weatherStation.update(new WeatherData(8, 32));
        System.out.println("\n");

        weatherStation.addObserver(homeAutomation);

        weatherStation.update(new WeatherData(8, 29));
        System.out.println("\n");

        weatherStation.update(new WeatherData(8, 27));
        System.out.println("\n");

        weatherStation.removeObserver(homeAutomation);

        weatherStation.update(new WeatherData(7, 25));

    }
}
