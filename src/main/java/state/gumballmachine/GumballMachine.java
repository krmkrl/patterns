package state.gumballmachine;

public class GumballMachine {

    private State noQuarterState;
    private State hasQuarterState;
    private State soldState;
    private State soldOutState;

    private State state;
    private int count = 0;

    public GumballMachine(int count) {
        noQuarterState = new NoQuarterState(this);
        hasQuarterState = new HasQuarterState(this);
        soldState = new SoldState(this);
        soldOutState = new SoldOutState(this);
        this.count = count;
        state = soldOutState;
        if (count > 0) {
            state = noQuarterState;
        }
    }

    public void insertQuarter() {
        state.insertQuarter();
    }

    public void ejectQuarter() {
        state.ejectQuarter();
    }

    public void turnCrank() {
        state.turnCrank();
    }

    public void refill(int count) {
        state.refill(count);
    }

    @Override
    public String toString() {
        return String.format("Gumball Machine 2000, count = %d %n%s%n", count, state.toString());
    }

    // methods not exposed to user below

    void onCrankTurnedWithQuarter() {
        state.dispense();
    }

    void toNoQuarterState() {
        state = noQuarterState;
    }

    void toHasQuarterState() {
        state = hasQuarterState;
    }

    void toSoldState() {
        state = soldState;
    }

    void toSoldOutState() {
        state = soldOutState;
    }

    void releaseBall() {
        System.out.println("A gumball comes rolling out...");
        if (count > 0) {
            count--;
        }
    }

    int getNumGumballs() {
        return count;
    }

    void addGumballs(int count) {
        this.count += count;
    }
}
