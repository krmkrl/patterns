package state.gumballmachine;

public class SoldOutState implements State {

    private final GumballMachine gumballMachine;

    public SoldOutState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void refill(int count) {
        System.out.println(String.format("Refilling gumball machine with %d gumballs", count));
        gumballMachine.addGumballs(count);
        gumballMachine.toNoQuarterState();
    }

    @Override
    public String toString() {
        return "Machine is sold out!";
    }
}
