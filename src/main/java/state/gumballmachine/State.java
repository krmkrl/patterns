package state.gumballmachine;

public interface State {
    default void insertQuarter() {
        System.out.println("Insert quarter not possible in current state");
    }

    default void ejectQuarter() {
        System.out.println("Eject quarter not possible in current state");
    }

    default void turnCrank() {
        System.out.println("Turn crank not possible in current state");
    }

    default void dispense() {
        System.out.println("Dispense not possible in current state");
    }

    default void refill(int count) {
        System.out.println("Refill not possible in current state");
    }
}
