package state.gumballmachine;

public class SoldState implements State {

    private final GumballMachine gumballMachine;

    public SoldState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void dispense() {
        gumballMachine.releaseBall();
        if (gumballMachine.getNumGumballs() > 0) {
            gumballMachine.toNoQuarterState();
        } else {
            System.out.println("Out of gumballs :(");
            gumballMachine.toSoldOutState();
        }
    }

    @Override
    public String toString() {
        return "Gumball sold";
    }
}
