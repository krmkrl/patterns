package state.gumballmachine;

public class HasQuarterState implements State {

    private final GumballMachine gumballMachine;

    public HasQuarterState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void ejectQuarter() {
        System.out.println("Quarter returned");
        gumballMachine.toNoQuarterState();
    }

    @Override
    public void turnCrank() {
        System.out.println("Crank turned!");
        gumballMachine.toSoldState();
        gumballMachine.onCrankTurnedWithQuarter();
    }

    @Override
    public String toString() {
        return "Quarter inserted";
    }
}
