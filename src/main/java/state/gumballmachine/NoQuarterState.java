package state.gumballmachine;

public class NoQuarterState implements State {

    private final GumballMachine gumballMachine;

    public NoQuarterState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void insertQuarter() {
        System.out.println("Quarter inserted");
        gumballMachine.toHasQuarterState();
    }

    @Override
    public String toString() {
        return "Waiting for quarter";
    }
}
