package compound.example.decorators;

import compound.example.Quackable;

import java.util.concurrent.atomic.AtomicInteger;

public class QuackCounterDecorator implements Quackable {

    private final Quackable duck;
    private static AtomicInteger numQuacks = new AtomicInteger(0);

    public QuackCounterDecorator(Quackable duck) {
        this.duck = duck;
    }

    @Override
    public String name() {
        return duck.name();
    }

    @Override
    public void quack() {
        duck.quack();
        numQuacks.incrementAndGet();
    }

    public static int getNumQuacks() {
        return numQuacks.get();
    }
}
