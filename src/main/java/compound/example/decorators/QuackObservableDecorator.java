package compound.example.decorators;

import compound.example.observer.QuackObserver;
import compound.example.Quackable;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class QuackObservableDecorator implements Quackable {

    private final Quackable duck;
    private final List<QuackObserver> observers = new CopyOnWriteArrayList<>();

    public QuackObservableDecorator(Quackable duck) {
        this.duck = duck;
    }

    @Override
    public void quack() {
        duck.quack();
        notifyObservers();
    }

    @Override
    public String name() {
        return duck.name();
    }

    @Override
    public void addObserver(QuackObserver observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(obs -> obs.update(duck));
    }
}
