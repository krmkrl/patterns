package compound.example;

import compound.example.observer.QuackObserver;

import java.util.HashMap;
import java.util.Map;

public class Quackologist implements QuackObserver {

    private final Map<String, Integer> duckTypeToNumQuacks = new HashMap<>();

    @Override
    public void update(Quackable quackable) {
        System.out.println("Quackologist: " + quackable.name() + " just quacked");
        duckTypeToNumQuacks.merge(quackable.name(), 1, Integer::sum);
    }

    public void quacksPerDuckType() {
        System.out.println(duckTypeToNumQuacks);
    }
}
