package compound.example.duckfactories;

import compound.example.Quackable;
import compound.example.decorators.QuackCounterDecorator;

public class CountingDuckFactory implements DuckFactory {

    private final DuckFactory factory;

    public CountingDuckFactory(DuckFactory factory) {
        this.factory = factory;
    }

    @Override
    public Quackable create(String name) {
        return new QuackCounterDecorator(factory.create(name));
    }
}
