package compound.example.duckfactories;

import compound.example.Quackable;
import compound.example.decorators.QuackObservableDecorator;

public class ObservableDuckFactory implements DuckFactory {

    private final DuckFactory factory;

    public ObservableDuckFactory(DuckFactory factory) {
        this.factory = factory;
    }

    @Override
    public Quackable create(String name) {
        return new QuackObservableDecorator(factory.create(name));
    }
}
