package compound.example.duckfactories;

import compound.example.Quackable;

public interface DuckFactory {
    Quackable create(String name);
}
