package compound.example.duckfactories;

import compound.example.Quackable;
import compound.example.ducks.DuckCall;
import compound.example.ducks.MallardDuck;
import compound.example.ducks.RedHeadDuck;
import compound.example.ducks.RubberDuck;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class DefaultDuckFactory implements DuckFactory {

    private final Map<String, Supplier<Quackable>> factoryMap = new HashMap<>();

    public DefaultDuckFactory() {
        factoryMap.put(DuckCall.class.getSimpleName(), DuckCall::new);
        factoryMap.put(MallardDuck.class.getSimpleName(), MallardDuck::new);
        factoryMap.put(RedHeadDuck.class.getSimpleName(), RedHeadDuck::new);
        factoryMap.put(RubberDuck.class.getSimpleName(), RubberDuck::new);
    }

    public Map<String, Supplier<Quackable>> getFactoryMap() {
        return new HashMap<>(factoryMap);
    }

    @Override
    public Quackable create(String name) {
        return factoryMap.getOrDefault(name, MallardDuck::new).get();
    }
}
