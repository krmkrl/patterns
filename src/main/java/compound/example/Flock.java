package compound.example;

import compound.example.observer.QuackObserver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Flock implements Quackable {

    private final List<Quackable> quackers = new ArrayList<>();

    public void add(Quackable... quackers) {
        this.quackers.addAll(Arrays.asList(quackers));
    }

    @Override
    public void quack() {
        quackers.forEach(Quackable::quack);
    }

    @Override
    public String name() {
        return quackers.stream()
                .map(Quackable::name)
                .collect(Collectors.joining(", "));
    }

    @Override
    public void addObserver(QuackObserver observer) {
        quackers.forEach(q -> q.addObserver(observer));
    }
}
