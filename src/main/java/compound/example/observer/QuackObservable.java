package compound.example.observer;

public interface QuackObservable {
    default void addObserver(QuackObserver observer) {}
    default void notifyObservers() {}
}
