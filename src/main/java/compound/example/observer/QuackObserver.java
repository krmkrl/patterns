package compound.example.observer;

import compound.example.Quackable;

public interface QuackObserver {
    void update(Quackable observable);
}
