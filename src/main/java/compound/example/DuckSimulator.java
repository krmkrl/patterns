package compound.example;

import compound.example.decorators.QuackCounterDecorator;
import compound.example.duckfactories.CountingDuckFactory;
import compound.example.duckfactories.DefaultDuckFactory;
import compound.example.duckfactories.DuckFactory;
import compound.example.duckfactories.ObservableDuckFactory;
import compound.example.ducks.DuckCall;
import compound.example.ducks.MallardDuck;
import compound.example.ducks.RedHeadDuck;
import compound.example.ducks.RubberDuck;
import compound.example.geese.Goose;
import compound.example.geese.GooseAdapter;

import java.util.stream.IntStream;

public class DuckSimulator {

    public static void main(String[] args) {
        DuckSimulator simulator = new DuckSimulator();

        DuckFactory duckFactory = new ObservableDuckFactory(new CountingDuckFactory(new DefaultDuckFactory()));
        simulator.simulate(duckFactory);
    }

    private void simulate(DuckFactory duckFactory) {
        Quackable redheadDuck = duckFactory.create(RedHeadDuck.class.getSimpleName());
        Quackable duckCall = duckFactory.create(DuckCall.class.getSimpleName());
        Quackable rubberDuck = duckFactory.create(RubberDuck.class.getSimpleName());
        Quackable gooseDuck = new GooseAdapter(new Goose()); // should not be counted or observed

        Flock ducks = new Flock();
        ducks.add(redheadDuck, duckCall, rubberDuck, gooseDuck);

        Flock mallards = new Flock();
        IntStream.range(0, 4).forEach(i -> mallards.add(duckFactory.create(MallardDuck.class.getSimpleName())));

        ducks.add(mallards);

        Quackologist quackologist = new Quackologist();
        ducks.addObserver(quackologist);

        System.out.println("Duck Simulator 1.0\n");

        System.out.println("All ducks: " + ducks.name());

        System.out.println("\nSimulate all ducks");
        simulate(ducks);

        System.out.println("\nSimulate mallards only\n");
        simulate(mallards);

        System.out.println("\nTotal num quacks: " + QuackCounterDecorator.getNumQuacks());
        quackologist.quacksPerDuckType();
    }

    private void simulate(Quackable quackable) {
        quackable.quack();
    }
}
