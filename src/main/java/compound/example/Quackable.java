package compound.example;

import compound.example.observer.QuackObservable;

public interface Quackable extends QuackObservable {

    void quack();

    default String name() {
        return getClass().getSimpleName();
    }
}
