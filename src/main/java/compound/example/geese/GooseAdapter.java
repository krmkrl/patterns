package compound.example.geese;

import compound.example.Quackable;

public class GooseAdapter implements Quackable {

    private final Goose goose;

    public GooseAdapter(Goose goose) {
        this.goose = goose;
    }

    @Override
    public String name() {
        return "GooseDuck";
    }

    @Override
    public void quack() {
        goose.honk();
    }
}
