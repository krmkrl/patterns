package proxy;

import java.math.BigInteger;

public class ComputeServiceImpl implements ComputeService {
    @Override
    public BigInteger fibonacci(int num) {
        if (num < 0) {
            throw new IllegalArgumentException("Input must be > 0");
        }
        return fibRecursive(BigInteger.valueOf(num));
    }

    private BigInteger fibRecursive(BigInteger num) {
        if (num.equals(BigInteger.ZERO) || num.equals(BigInteger.ONE)) {
            return num;
        } else {
            return fibRecursive(num.subtract(BigInteger.ONE)).add(fibRecursive(num.subtract(BigInteger.TWO)));
        }
    }

    @Override
    public BigInteger factorial(int num) {
        if (num < 0) {
            throw new IllegalArgumentException("Input must be > 0");
        }
        return facRecursive(BigInteger.valueOf(num));
    }

    private BigInteger facRecursive(BigInteger num) {
        if (num.equals(BigInteger.ZERO)) {
            return BigInteger.ONE;
        } else {
            return num.multiply(facRecursive(num.subtract(BigInteger.ONE)));
        }
    }
}
