package proxy;

import java.math.BigInteger;

public interface ComputeService {

    BigInteger fibonacci(int num);
    BigInteger factorial(int num);
}
