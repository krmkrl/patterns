package proxy;

import java.math.BigInteger;

public class Mathematician {

    private final String name;
    private final ComputeService computeService;

    public Mathematician(String name, ComputeService computeService) {
        this.name = name;
        this.computeService = computeService;
    }

    public void calculateFibonacci(int num) {
        long start = System.currentTimeMillis();
        BigInteger result = computeService.fibonacci(num);
        long end = System.currentTimeMillis();

        System.out.println(String.format("%s computed fib(%d) = %d \n it took %d ms \n", name, num, result, end - start));
    }

    public void calculateFactorial(int num) {
        long start = System.currentTimeMillis();
        BigInteger result = computeService.factorial(num);
        long end = System.currentTimeMillis();

        System.out.println(String.format("%s computed fac(%d) = %d \n it took %d ms \n", name, num, result, end - start));
    }

    public static void main(String[] args) {
        ComputeService computeService = new ComputeServiceProxy(new ComputeServiceImpl());

        Mathematician pythagoras = new Mathematician("Pythagoras", computeService);
        Mathematician fermat = new Mathematician("Fermat", computeService);
        Mathematician newton = new Mathematician("Newton", computeService);

        pythagoras.calculateFactorial(580);
        pythagoras.calculateFibonacci(38);

        fermat.calculateFibonacci(29);
        fermat.calculateFibonacci(38);

        newton.calculateFactorial(580);
        newton.calculateFibonacci(32);
    }
}
