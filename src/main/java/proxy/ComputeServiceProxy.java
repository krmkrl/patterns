package proxy;

import java.math.BigInteger;
import java.util.HashMap;

public class ComputeServiceProxy implements ComputeService {
    private final ComputeService computeService;

    private final HashMap<String, BigInteger> computeCache = new HashMap<>();

    public ComputeServiceProxy(ComputeService computeService) {
        this.computeService = computeService;
    }

    @Override
    public BigInteger fibonacci(int num) {
        String key = "fib" + num;
        return computeCache.computeIfAbsent(key, k -> computeService.fibonacci(num));
    }

    @Override
    public BigInteger factorial(int num) {
        String key = "fac" + num;
        return computeCache.computeIfAbsent(key, k -> computeService.factorial(num));
    }
}
