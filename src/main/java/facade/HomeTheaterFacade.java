package facade;

import facade.movieroom.*;

public class HomeTheaterFacade {

    private final Amplifier amplifier;
    private final DvdPlayer dvdPlayer;
    private final Lights lights;
    private final PopcornMachine popcornMachine;
    private final Projector projector;
    private final Screen screen;

    public HomeTheaterFacade(Amplifier amplifier, DvdPlayer dvdPlayer, Lights lights, PopcornMachine popcornMachine, Projector projector, Screen screen) {
        this.amplifier = amplifier;
        this.dvdPlayer = dvdPlayer;
        this.lights = lights;
        this.popcornMachine = popcornMachine;
        this.projector = projector;
        this.screen = screen;
    }

    public void watchMovie(String movie) {
        System.out.println("Get ready to watch movie: " + movie);

        popcornMachine.on();
        popcornMachine.pop();
        lights.dim();
        screen.down();
        projector.on();
        projector.wideScreenMode();
        amplifier.on();
        amplifier.setDVD();
        amplifier.setSurroundSound();
        amplifier.setVolume(25);
        dvdPlayer.on();
        dvdPlayer.play(movie);
    }

    public void endMovie() {
        System.out.println("Shutting movie theater down ...");

        popcornMachine.off();
        lights.on();
        screen.up();
        projector.off();
        amplifier.off();
        dvdPlayer.stop();
        dvdPlayer.eject();
        dvdPlayer.off();
    }
}
