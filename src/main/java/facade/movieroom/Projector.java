package facade.movieroom;

public class Projector {
    public void on() {
        System.out.println("Turning Projector ON");
    }

    public void off() {
        System.out.println("Turning Projector OFF");
    }

    public void tvMode() {
        System.out.println("Setting Projector to tv mode");
    }

    public void wideScreenMode() {
        System.out.println("Setting Projector to widescreen mode");
    }
}
