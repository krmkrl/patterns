package facade.movieroom;

public class DvdPlayer {
    public void on() {
        System.out.println("Turning Dvd player ON");
    }

    public void off() {
        System.out.println("Turning Dvd player OFF");
    }

    public void play(String movie) {
        System.out.println("Dvd player playing movie: " + movie);
    }

    public void pause() {
        System.out.println("Pausing ...");
    }

    public void eject() {
        System.out.println("Ejecting movie ...");
    }

    public void stop() {
        System.out.println("Movie stopped.");
    }
}
