package facade.movieroom;

public class Amplifier {

    public void on() {
        System.out.println("Turning Amplifier ON");
    }

    public void off() {
        System.out.println("Turning Amplifier OFF");
    }

    public void setCD() {
        System.out.println("Amplifier input set to CD");
    }

    public void setDVD() {
        System.out.println("Amplifier input set to DVD");
    }

    public void setSurroundSound() {
        System.out.println("Amplifier setting surround sound");
    }

    public void setStereoSound() {
        System.out.println("Amplifier setting stereo sound");
    }

    public void setVolume(int volume) {
        System.out.println("Amplifier setting volume to: " + volume);
    }
}
