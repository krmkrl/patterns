package facade.movieroom;

public class Lights {
    public void on() {
        System.out.println("Turning Lights ON");
    }

    public void off() {
        System.out.println("Turning Lights OFF");
    }

    public void dim() {
        System.out.println("Dimming lights");
    }

}
