package facade.movieroom;

public class PopcornMachine {
    public void on() {
        System.out.println("Turning Popcorn Machine ON");
    }

    public void off() {
        System.out.println("Turning Popcorn Machine OFF");
    }

    public void pop() {
        System.out.println("Popcorn Machine is now popping delicious popcorn");
    }
}
