package facade;

import facade.movieroom.*;

public class Client {

    public static void main(String[] args) {
        Amplifier amplifier = new Amplifier();
        DvdPlayer dvdPlayer = new DvdPlayer();
        Lights lights = new Lights();
        PopcornMachine popcornMachine = new PopcornMachine();
        Projector projector = new Projector();
        Screen screen = new Screen();

        HomeTheaterFacade homeTheaterFacade = new HomeTheaterFacade(amplifier, dvdPlayer, lights, popcornMachine, projector, screen);

        homeTheaterFacade.watchMovie("Lucky Number Slevin");

        System.out.println();

        homeTheaterFacade.endMovie();
    }
}
