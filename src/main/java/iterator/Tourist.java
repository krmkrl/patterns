package iterator;

public class Tourist {
    public static void main(String[] args) {
        System.out.println("On the plane ... \n");

        useTinyDictionary();

        System.out.println("\nGoing to Santiago Bernabeu! \n");

        useSoccerDictionary();
    }

    private static void useTinyDictionary() {
        SpanishDictionary tinyDict = SpanishDictionaryMaker.getFreeTinyDictionary();

        String trans = tinyDict.getTranslation("Hola").orElseThrow();
        System.out.println("Hola means " + trans);

        System.out.println("Reading entire tiny dictionary...");
        for (SpanishWord word : tinyDict) {
            System.out.println(word);
        }
    }

    private static void useSoccerDictionary() {
        SpanishDictionary soccerDict = SpanishDictionaryMaker.buyExpensiveSoccerDictionary();

        String trans = soccerDict.getTranslation("Fútbol").orElseThrow();
        System.out.println("Fútbol means " + trans);

        System.out.println("Reading entire soccer dictionary...");
        soccerDict.stream().forEach(System.out::println);
    }
}
