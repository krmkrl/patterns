package iterator.dictionaries;

import iterator.SpanishWord;

import java.util.Iterator;

public class TinySpanishDictionaryIterator implements Iterator<SpanishWord> {

    private final SpanishWord[] words;
    private int pos = 0;

    public TinySpanishDictionaryIterator(SpanishWord[] words) {
        SpanishWord[] wordsCopy = new SpanishWord[words.length];
        System.arraycopy(words, 0, wordsCopy, 0, words.length);
        this.words = wordsCopy;
    }

    @Override
    public boolean hasNext() {
        return pos < words.length && words[pos] != null;
    }

    @Override
    public SpanishWord next() {
        return words[pos++];
    }
}
