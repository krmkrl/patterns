package iterator.dictionaries;

import iterator.SpanishDictionary;
import iterator.SpanishWord;

import java.util.Iterator;
import java.util.Optional;

public class TinySpanishDictionary implements SpanishDictionary {
    private static final int MAX_DICT_SIZE = 5;
    private int numItems = 0;
    private SpanishWord[] words = new SpanishWord[MAX_DICT_SIZE];

    @Override
    public void addWord(SpanishWord word) {
        if (numItems >= MAX_DICT_SIZE) {
            throw new RuntimeException("Dictionary is full!");
        }
        words[numItems++] = word;
    }

    @Override
    public Optional<String> getTranslation(String spanishWord) {
        if (spanishWord == null) {
            return Optional.empty();
        }

        for (int i = 0; i < words.length; i++) {
            SpanishWord word = words[i];
            if (word != null && word.getWord().equals(spanishWord)) {
                return Optional.ofNullable(word.getTranslation());
            }
        }
        return Optional.empty();
    }

    @Override
    public Iterator<SpanishWord> iterator() {
        return new TinySpanishDictionaryIterator(words);
    }

}
