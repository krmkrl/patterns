package iterator.dictionaries;

import iterator.SpanishDictionary;
import iterator.SpanishWord;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class SpanishSoccerDictionary implements SpanishDictionary {

    private final List<SpanishWord> words = new ArrayList<>();

    @Override
    public void addWord(SpanishWord word) {
        words.add(word);
    }

    @Override
    public Optional<String> getTranslation(String spanishWord) {
        if (spanishWord == null) {
            return Optional.empty();
        }
        for (SpanishWord word : words) {
            if (word != null && word.getWord().equals(spanishWord)) {
                return Optional.ofNullable(word.getTranslation());
            }
        }
        return Optional.empty();
    }

    @Override
    public Iterator<SpanishWord> iterator() {
        return words.iterator();
    }
}
