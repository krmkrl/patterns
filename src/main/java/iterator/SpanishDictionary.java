package iterator;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface SpanishDictionary extends Iterable<SpanishWord> {
    void addWord(SpanishWord word);

    Optional<String> getTranslation(String spanishWord);

    default Stream<SpanishWord> stream() {
        return StreamSupport.stream(spliterator(), false);
    }
}
