package iterator;

import iterator.dictionaries.SpanishSoccerDictionary;
import iterator.dictionaries.TinySpanishDictionary;

public class SpanishDictionaryMaker {

    public static SpanishDictionary getFreeTinyDictionary() {
        SpanishDictionary dict = new TinySpanishDictionary();
        dict.addWord(new SpanishWord("Hola", "Hello"));
        dict.addWord(new SpanishWord("Perdon", "Excuse me"));
        dict.addWord(new SpanishWord("Dónde?", "Where?"));
        dict.addWord(new SpanishWord("Mañana", "Tomorrow"));
        dict.addWord(new SpanishWord("Mas cerveza", "More beer"));
        return dict;
    }

    public static SpanishDictionary buyExpensiveSoccerDictionary() {
        SpanishDictionary dict = new SpanishSoccerDictionary();
        dict.addWord(new SpanishWord("Fútbol", "Soccer"));
        dict.addWord(new SpanishWord("Jugador de fútbol", "Soccer player"));
        dict.addWord(new SpanishWord("Gol", "Goal"));
        dict.addWord(new SpanishWord("Capitán", "Captain"));
        dict.addWord(new SpanishWord("Defensa", "Defender"));
        dict.addWord(new SpanishWord("Mediocampista", "Midfielder"));
        dict.addWord(new SpanishWord("Delantero", "Forward"));

        return dict;
    }
}
