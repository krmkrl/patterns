package command.commands.tv;

import command.Command;
import command.smartproducts.TV;

public class TurnOffTVCommand implements Command {
    private final TV tv;

    public TurnOffTVCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.off();
    }
}
