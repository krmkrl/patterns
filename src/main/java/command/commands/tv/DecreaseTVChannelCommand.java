package command.commands.tv;

import command.Command;
import command.smartproducts.TV;

public class DecreaseTVChannelCommand implements Command {
    private final TV tv;

    public DecreaseTVChannelCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        int currentChannel = tv.getChannel();
        tv.setChannel(currentChannel - 1);
    }
}
