package command.commands.tv;

import command.Command;
import command.smartproducts.TV;

public class IncreaseTVChannelCommand implements Command {
    private final TV tv;

    public IncreaseTVChannelCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        int currentChannel = tv.getChannel();
        tv.setChannel(currentChannel + 1);
    }
}
