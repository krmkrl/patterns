package command.commands.tv;

import command.Command;
import command.smartproducts.TV;

public class IncreaseTVVolumeCommand implements Command {
    private final TV tv;

    public IncreaseTVVolumeCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        int currentVolume = tv.getVolume();
        tv.setVolume(currentVolume + 1);
    }
}
