package command.commands.tv;

import command.Command;
import command.smartproducts.TV;

public class TurnOnTVCommand implements Command {
    private final TV tv;

    public TurnOnTVCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.on();
    }
}
