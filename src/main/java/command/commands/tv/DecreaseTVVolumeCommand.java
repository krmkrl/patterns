package command.commands.tv;

import command.Command;
import command.smartproducts.TV;

public class DecreaseTVVolumeCommand implements Command {
    private final TV tv;

    public DecreaseTVVolumeCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        int currentVolume = tv.getVolume();
        tv.setVolume(currentVolume - 1);
    }
}
