package command.commands.stereo;

import command.Command;
import command.smartproducts.Stereo;

public class TurnOnStereoCommand implements Command {
    private final Stereo stereo;

    public TurnOnStereoCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    @Override
    public void execute() {
        stereo.on();
    }
}
