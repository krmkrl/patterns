package command.commands.stereo;

import command.Command;
import command.smartproducts.Stereo;

public class DecreaseStereoVolumeCommand implements Command {
    private final Stereo stereo;

    public DecreaseStereoVolumeCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    @Override
    public void execute() {
        int currentVolume = stereo.getVolume();
        stereo.setVolume(currentVolume - 1);
    }
}
