package command.commands.stereo;

import command.Command;
import command.smartproducts.Stereo;

public class IncreaseStereoVolumeCommand implements Command {
    private final Stereo stereo;

    public IncreaseStereoVolumeCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    @Override
    public void execute() {
        int currentVolume = stereo.getVolume();
        stereo.setVolume(currentVolume + 1);
    }
}
