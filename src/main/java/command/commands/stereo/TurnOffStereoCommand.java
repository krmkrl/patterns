package command.commands.stereo;

import command.Command;
import command.smartproducts.Stereo;

public class TurnOffStereoCommand implements Command {
    private final Stereo stereo;

    public TurnOffStereoCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    @Override
    public void execute() {
        stereo.off();
    }
}
