package command.commands;

import command.Command;

public class EmptyCommand implements Command {
    @Override
    public void execute() {
        // noop
    }
}
