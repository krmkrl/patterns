package command.commands.hottub;

import command.Command;
import command.smartproducts.Hottub;

public class TurnOnHottubCommand implements Command {
    private final Hottub hottub;

    public TurnOnHottubCommand(Hottub hottub) {
        this.hottub = hottub;
    }

    @Override
    public void execute() {
        hottub.on();
    }
}
