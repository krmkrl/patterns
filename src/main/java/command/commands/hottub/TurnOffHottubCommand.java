package command.commands.hottub;

import command.Command;
import command.smartproducts.Hottub;

public class TurnOffHottubCommand implements Command {
    private final Hottub hottub;

    public TurnOffHottubCommand(Hottub hottub) {
        this.hottub = hottub;
    }

    @Override
    public void execute() {
        hottub.off();
    }
}
