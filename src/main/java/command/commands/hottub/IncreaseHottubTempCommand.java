package command.commands.hottub;

import command.Command;
import command.smartproducts.Hottub;

public class IncreaseHottubTempCommand implements Command {
    private final Hottub hottub;

    public IncreaseHottubTempCommand(Hottub hottub) {
        this.hottub = hottub;
    }

    @Override
    public void execute() {
        int currentTemp = hottub.getTemperature();
        hottub.setTemperature(currentTemp + 1);
    }
}
