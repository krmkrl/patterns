package command.commands.hottub;

import command.Command;
import command.smartproducts.Hottub;

public class DecreaseHottubTempCommand  implements Command {
    private final Hottub hottub;

    public DecreaseHottubTempCommand(Hottub hottub) {
        this.hottub = hottub;
    }

    @Override
    public void execute() {
        int currentTemp = hottub.getTemperature();
        hottub.setTemperature(currentTemp - 1);
    }
}
