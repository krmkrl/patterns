package command.smartproducts;

public class TV {
    private int channel;
    private int volume;

    public void on() {
        System.out.println("Turning ON TV, channel " + channel + " volume = " + volume);
    }

    public void off() {
        System.out.println("Turning OFF TV");
    }

    public void setChannel(int channel) {
        System.out.println("Setting TV channel to " + channel);
        this.channel = channel;
    }

    public int getChannel() {
        return channel;
    }

    public void setVolume(int volume) {
        System.out.println("Setting TV volume to " + volume);
        this.volume = volume;
    }

    public int getVolume() {
        return volume;
    }
}
