package command.smartproducts;

public class Stereo {
    private int volume;

    public void on() {
        System.out.println("Turning ON stereo, volume = " + volume);
    }

    public void off() {
        System.out.println("Turning OFF stereo");
    }

    public void setVolume(int volume) {
        System.out.println("Setting stereo volume to " + volume);
        this.volume = volume;
    }

    public int getVolume() {
        return volume;
    }
}
