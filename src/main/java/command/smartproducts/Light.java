package command.smartproducts;

public class Light {
    private String room;

    public Light(String room) {
        this.room = room;
    }

    public void on() {
        System.out.println("Turning ON light in " + room);
    }

    public void off() {
        System.out.println("Turning OFF light in " + room);
    }

}
