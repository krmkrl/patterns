package command.smartproducts;

public class Hottub {
    private int temperature;

    public void on() {
        System.out.println("Turning ON hottub, temperature is " + temperature + " degrees");
    }

    public void off() {
        System.out.println("Turning OFF hottub");
    }

    public void setTemperature(int temperature) {
        System.out.println("Setting temperature to " + temperature + " degrees");
        this.temperature = temperature;
    }

    public int getTemperature() {
        return temperature;
    }
}
