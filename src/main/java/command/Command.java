package command;

@FunctionalInterface
public interface Command {
    void execute();

    // Another common usage is to include an undo() method that every command has to implement.
    // But then the interface is no longer functional.
}
