package command;

import command.commands.MacroCommand;
import command.commands.hottub.IncreaseHottubTempCommand;
import command.commands.hottub.TurnOnHottubCommand;
import command.commands.light.TurnOffLightCommand;
import command.commands.light.TurnOnLightCommand;
import command.commands.stereo.DecreaseStereoVolumeCommand;
import command.commands.stereo.IncreaseStereoVolumeCommand;
import command.commands.stereo.TurnOffStereoCommand;
import command.commands.stereo.TurnOnStereoCommand;
import command.commands.tv.TurnOffTVCommand;
import command.smartproducts.Hottub;
import command.smartproducts.Light;
import command.smartproducts.Stereo;
import command.smartproducts.TV;

import java.util.List;

public class RemoteLoader {
    public static void main(String[] args) {
        RemoteControl remote = new RemoteControl(8);

        remote.buttonPressed(0);

        System.out.println(remote);

        Light kitchenLight = new Light("Kitchen");
        Light livingRoomLight = new Light("Livingroom");
        Hottub hottub = new Hottub();
        Stereo stereo = new Stereo();
        TV tv = new TV();

        TurnOnLightCommand turnOnKitchenLight = new TurnOnLightCommand(kitchenLight);
        TurnOffLightCommand turnOffKitchenLight = new TurnOffLightCommand(kitchenLight);

        TurnOnStereoCommand turnOnStereo = new TurnOnStereoCommand(stereo);
        TurnOffStereoCommand turnOffStereo = new TurnOffStereoCommand(stereo);
        IncreaseStereoVolumeCommand increaseStereoVolume = new IncreaseStereoVolumeCommand(stereo);
        DecreaseStereoVolumeCommand decreaseStereoVolume = new DecreaseStereoVolumeCommand(stereo);

        Command turnOnTvOnChannel5Command = () -> {tv.on(); tv.setChannel(5); tv.setVolume(11);};

        TurnOnHottubCommand turnOnHottub = new TurnOnHottubCommand(hottub);
        IncreaseHottubTempCommand increaseHottubTemp = new IncreaseHottubTempCommand(hottub);
        TurnOffTVCommand turnOffTV = new TurnOffTVCommand(tv);
        TurnOnLightCommand turnOnLivingroomLight = new TurnOnLightCommand(livingRoomLight);

        MacroCommand partyTime = new MacroCommand(List.of(turnOnLivingroomLight, turnOffTV, turnOnStereo, increaseStereoVolume, turnOnHottub, increaseHottubTemp, increaseHottubTemp));

        remote.setCommand(0, turnOnKitchenLight);
        remote.setCommand(1, turnOffKitchenLight);
        remote.setCommand(2, turnOnStereo);
        remote.setCommand(3, turnOffStereo);
        remote.setCommand(4, increaseStereoVolume);
        remote.setCommand(5, decreaseStereoVolume);
        remote.setCommand(6, turnOnTvOnChannel5Command);
        remote.setCommand(7, partyTime);

        System.out.println(remote);

        remote.buttonPressed(0);
        remote.buttonPressed(2);
        remote.buttonPressed(4);
        remote.buttonPressed(4);
        remote.buttonPressed(3);
        remote.buttonPressed(2);
        remote.buttonPressed(6);

        System.out.println("\nIt's party time! \n");
        remote.buttonPressed(7);
    }
}
