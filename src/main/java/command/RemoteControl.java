package command;

import command.commands.EmptyCommand;

public class RemoteControl {

    private final Command[] commands;

    public RemoteControl(int numButtons) {
        commands = new Command[numButtons];
        for (int i = 0; i < commands.length; i++) {
            commands[i] = new EmptyCommand();
        }
    }

    public void setCommand(int pos, Command command) {
        commands[pos] = command;
    }

    public void buttonPressed(int pos) {
        commands[pos].execute();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("Remote control with " + commands.length + " buttons: \n");
        for (Command command : commands) {
            builder.append("Command " + command.getClass().getSimpleName() + "\n");
        }

        return builder.toString();
    }
}
