package templatemethod.java8;

@FunctionalInterface
public interface ColdBeverage {

    default void makeBeverage() {
        makeIce();
        addCondiments();
        mixInBlender();
        ready();
    }

    default void makeIce() {
        System.out.println("Freezing water into cubes");
    }

    default void mixInBlender() {
        System.out.println("Mixing in blender for 10 seconds ...");
    }

    void addCondiments();

    default void ready() {
        System.out.println("Cold beverage is ready");
    }
}
