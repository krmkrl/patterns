package templatemethod.java8;

public class StrawberryFrappe implements ColdBeverage {
    @Override
    public void addCondiments() {
        System.out.println("Adding fresh strawberries");
    }
}
