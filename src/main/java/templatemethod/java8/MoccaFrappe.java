package templatemethod.java8;

public class MoccaFrappe implements ColdBeverage {
    @Override
    public void addCondiments() {
        System.out.println("Adding chocolate nibs");
    }

    @Override
    public void ready() {
        System.out.println("Mocca Frappe is ready!");
    }
}
