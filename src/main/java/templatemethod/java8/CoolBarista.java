package templatemethod.java8;

public class CoolBarista {
    public static void main(String[] args) {
        System.out.println("Making a Mocca Frappe ...");
        MoccaFrappe moccaFrappe = new MoccaFrappe();
        moccaFrappe.makeBeverage();

        System.out.println();

        System.out.println("Making a Strawberry Frappe ...");
        StrawberryFrappe strawberryFrappe = new StrawberryFrappe();
        strawberryFrappe.makeBeverage();

        System.out.println();

        System.out.println("Making lambda frappe with my new portable Frappe machine");
        FrappeMachine.makeFrappe(() -> System.out.println("Adding lambda sprinkles"));
    }

}

class FrappeMachine {
    public static void makeFrappe(ColdBeverage coldBeverage) {
        coldBeverage.makeIce();
        coldBeverage.addCondiments();
        coldBeverage.mixInBlender();
        coldBeverage.ready();
    }
}
