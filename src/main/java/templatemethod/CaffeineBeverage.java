package templatemethod;

public abstract class CaffeineBeverage {

    public final void makeBeverage() {
        boilWater();
        brew();
        pourInCup();
        addCondiments();
        beverageReady();
    }

    protected abstract void brew();

    protected abstract void addCondiments();

    protected void beverageReady() { }

    private void boilWater() {
        System.out.println("Boiling water");
    }

    private void pourInCup() {
        System.out.println("Pouring into cup");
    }
}
