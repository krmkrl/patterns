package templatemethod;

public class Barista {

    public static void main(String[] args) {
        System.out.println("Making coffee...");
        Coffee coffe = new Coffee();
        coffe.makeBeverage();

        System.out.println();

        System.out.println("Making tea...");
        Tea tea = new Tea();
        tea.makeBeverage();
    }
}
