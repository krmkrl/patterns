package factory.abstractf.ingredients;

import factory.abstractf.ingredients.cheese.Cheese;
import factory.abstractf.ingredients.dough.Dough;
import factory.abstractf.ingredients.sauce.Sauce;

public interface PizzaIngredientFactory {
    Dough createDough();
    Sauce createSauce();
    Cheese createCheese();
}
