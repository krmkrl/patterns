package factory.abstractf.ingredients.sauce;

public class MarinaraSauce implements Sauce {
    @Override
    public String toString() {
        return "Marinara";
    }
}
