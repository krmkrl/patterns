package factory.abstractf.ingredients.cheese;

public class VegCheese implements Cheese {
    @Override
    public String toString() {
        return "Veg";
    }
}
