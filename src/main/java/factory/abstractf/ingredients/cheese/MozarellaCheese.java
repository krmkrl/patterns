package factory.abstractf.ingredients.cheese;

public class MozarellaCheese implements Cheese {

    @Override
    public String toString() {
        return "Mozarella";
    }
}
