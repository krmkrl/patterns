package factory.abstractf.region.chicago.factorytypes;

import factory.abstractf.ingredients.PizzaIngredientFactory;
import factory.abstractf.ingredients.cheese.Cheese;
import factory.abstractf.ingredients.cheese.VegCheese;
import factory.abstractf.ingredients.dough.Dough;
import factory.abstractf.ingredients.dough.ThickCrustDough;
import factory.abstractf.ingredients.sauce.PlumTomatoSauce;
import factory.abstractf.ingredients.sauce.Sauce;

public class ChicagoVegPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new ThickCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new PlumTomatoSauce();
    }

    @Override
    public Cheese createCheese() {
        return new VegCheese();
    }
}
