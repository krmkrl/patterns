package factory.abstractf.region.chicago;


import factory.abstractf.Pizza;
import factory.abstractf.PizzaFactory;
import factory.abstractf.region.chicago.factorytypes.ChicagoCheesePizzaIngredientFactory;
import factory.abstractf.region.chicago.factorytypes.ChicagoVegPizzaIngredientFactory;

public class ChicagoPizzaFactory extends PizzaFactory {
    @Override
    protected Pizza createPizza(PizzaType pizzaType) {
        if (pizzaType.equals(PizzaType.CHEESE)) {
            return new ChicagoStylePizza("Chicago Cheese Pizza", new ChicagoCheesePizzaIngredientFactory());
        } else if (pizzaType.equals(PizzaType.VEG)) {
            return new ChicagoStylePizza("Chicago Veggie Pizza", new ChicagoVegPizzaIngredientFactory());
        } else {
            return null;
        }
    }
}
