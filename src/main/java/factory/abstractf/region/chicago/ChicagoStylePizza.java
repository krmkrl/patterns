package factory.abstractf.region.chicago;

import factory.abstractf.Pizza;
import factory.abstractf.ingredients.PizzaIngredientFactory;

class ChicagoStylePizza extends Pizza {
    ChicagoStylePizza(String name, PizzaIngredientFactory ingredientFactory) {
        super(name, ingredientFactory.createDough(), ingredientFactory.createSauce(), ingredientFactory.createCheese());
    }

    @Override
    public void cut() {
        System.out.println("Cutting the pizza into square slices");
    }
}
