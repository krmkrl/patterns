package factory.abstractf.region.ny;

import factory.abstractf.Pizza;
import factory.abstractf.ingredients.PizzaIngredientFactory;

public class NYStylePizza extends Pizza {
    public NYStylePizza(String name, PizzaIngredientFactory ingredientFactory) {
        super(name, ingredientFactory.createDough(), ingredientFactory.createSauce(), ingredientFactory.createCheese());
    }

    @Override
    public void bake() {
        System.out.println("Baking pizza for 15 minutes at 350 degrees");
    }
}
