package factory.abstractf.region.ny;


import factory.abstractf.Pizza;
import factory.abstractf.PizzaFactory;
import factory.abstractf.region.ny.factorytypes.NYCheesePizzaIngredientFactory;
import factory.abstractf.region.ny.factorytypes.NYVegPizzaIngredientFactory;

public class NYPizzaFactory extends PizzaFactory {
    @Override
    protected Pizza createPizza(PizzaType pizzaType) {
        if (pizzaType.equals(PizzaType.CHEESE)) {
            return new NYStylePizza("NY Cheese Pizza", new NYCheesePizzaIngredientFactory());
        } else if (pizzaType.equals(PizzaType.VEG)) {
            return new NYStylePizza("NY Veggie Pizza", new NYVegPizzaIngredientFactory());
        } else {
            return null;
        }
    }
}
