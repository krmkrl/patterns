package factory.abstractf.region.ny.factorytypes;

import factory.abstractf.ingredients.PizzaIngredientFactory;
import factory.abstractf.ingredients.cheese.Cheese;
import factory.abstractf.ingredients.cheese.VegCheese;
import factory.abstractf.ingredients.dough.Dough;
import factory.abstractf.ingredients.dough.ThinCrustDough;
import factory.abstractf.ingredients.sauce.MarinaraSauce;
import factory.abstractf.ingredients.sauce.Sauce;

public class NYVegPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new ThinCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new MarinaraSauce();
    }

    @Override
    public Cheese createCheese() {
        return new VegCheese();
    }
}
