package factory.abstractf;

import factory.abstractf.region.chicago.ChicagoPizzaFactory;
import factory.abstractf.region.ny.NYPizzaFactory;

public class Main {
    public static void main(String[] args) {
        Pizza nyCheesePizza = new NYPizzaFactory().orderPizza(PizzaFactory.PizzaType.CHEESE);
        System.out.println("Cersei ordered a " + nyCheesePizza.getName());

        System.out.println("");

        Pizza chicagoVegPizza = new ChicagoPizzaFactory().orderPizza(PizzaFactory.PizzaType.VEG);
        System.out.println("Jon ordered a " + chicagoVegPizza.getName());
    }
}
