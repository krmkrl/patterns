package factory.abstractf;

import factory.abstractf.ingredients.cheese.Cheese;
import factory.abstractf.ingredients.dough.Dough;
import factory.abstractf.ingredients.sauce.Sauce;

public abstract class Pizza {

    private final String name;
    private final Dough dough;
    private final Sauce sauce;
    private final Cheese cheese;

    public Pizza(String name, Dough dough, Sauce sauce, Cheese cheese) {
        this.name = name;
        this.dough = dough;
        this.sauce = sauce;
        this.cheese = cheese;
    }

    public void prepare() {
        System.out.println("Preparing " + name);
        System.out.println("Tossing dough " + dough + "...");
        System.out.println("Adding sauce " + sauce + "...");
        System.out.println("Adding cheese " + cheese + "...");
    }

    public void bake() {
        System.out.println("Baking pizza for 25 minutes at 350 degrees");
    }

    public void cut() {
        System.out.println("Cutting pizza in diagonal slices");
    }

    public void box() {
        System.out.println("Boxing pizza in standard box");
    }

    public String getName() {
        return name;
    }
}
