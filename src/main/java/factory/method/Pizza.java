package factory.method;

import java.util.List;

public abstract class Pizza {

    private final String name;
    private final String dough;
    private final String sauce;
    private final List<String> toppings;

    public Pizza(String name, String dough, String sauce, List<String> toppings) {
        this.name = name;
        this.dough = dough;
        this.sauce = sauce;
        this.toppings = toppings;
    }

    public void prepare() {
        System.out.println("Preparing " + name);
        System.out.println("Tossing dough " + dough + "...");
        System.out.println("Adding sauce " + sauce + "...");
        System.out.println("Adding toppings: " + String.join(", ", toppings));
    }

    public void bake() {
        System.out.println("Baking pizza for 25 minutes at 350 degrees");
    }

    public void cut() {
        System.out.println("Cutting pizza in diagonal slices");
    }

    public void box() {
        System.out.println("Boxing pizza in standard box");
    }

    public String getName() {
        return name;
    }
}
