package factory.method.region.ny;

import factory.method.Pizza;

import java.util.List;

abstract class NYStylePizza extends Pizza {
    NYStylePizza(String name, String dough, String sauce, List<String> toppings) {
        super(name, dough, sauce, toppings);
    }

    @Override
    public void bake() {
        System.out.println("Baking pizza for 15 minutes at 350 degrees");
    }
}
