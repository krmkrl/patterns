package factory.method.region.ny;

import java.util.List;

class NYStyleCheesePizza extends NYStylePizza {
    NYStyleCheesePizza() {
        super("NY Style Sauce and Cheese Pizza", "Thin Crust Dough", "Marinara sauce", List.of("Reggiano Cheese"));
    }
}
