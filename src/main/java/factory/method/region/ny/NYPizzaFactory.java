package factory.method.region.ny;

import factory.method.Pizza;
import factory.method.PizzaFactory;

public class NYPizzaFactory extends PizzaFactory {
    @Override
    protected Pizza createPizza(PizzaType pizzaType) {
        if (pizzaType.equals(PizzaType.CHEESE)) {
            return new NYStyleCheesePizza();
        } else if (pizzaType.equals(PizzaType.VEG)) {
            return new NYStyleVegPizza();
        } else {
            return null;
        }
    }
}
