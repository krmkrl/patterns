package factory.method.region.ny;

import java.util.List;

class NYStyleVegPizza extends NYStylePizza {
    NYStyleVegPizza() {
        super("NY Style Veg Pizza", "Thin Crust Dough", "Marinara sauce", List.of("Veg Cheese", "Artichoke"));
    }
}
