package factory.method.region.chicago;

import factory.method.Pizza;
import factory.method.PizzaFactory;

public class ChicagoPizzaFactory extends PizzaFactory {
    @Override
    protected Pizza createPizza(PizzaType pizzaType) {
        if (pizzaType.equals(PizzaType.CHEESE)) {
            return new ChicagoStyleCheesePizza();
        } else if (pizzaType.equals(PizzaType.VEG)) {
            return new ChicagoStyleVegPizza();
        } else {
            return null;
        }
    }
}
