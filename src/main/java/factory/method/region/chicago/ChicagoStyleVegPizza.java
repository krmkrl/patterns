package factory.method.region.chicago;

import java.util.List;

class ChicagoStyleVegPizza extends ChicagoStylePizza {
    ChicagoStyleVegPizza() {
        super("Chicago Style Veg Pizza", "Thick Crust Dough", "Plum tomato sauce", List.of("Veg Cheese", "Artichoke"));
    }
}
