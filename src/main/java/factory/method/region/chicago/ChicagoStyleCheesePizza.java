package factory.method.region.chicago;

import java.util.List;

class ChicagoStyleCheesePizza extends ChicagoStylePizza {
    ChicagoStyleCheesePizza() {
        super("Chicago Style Sauce and Cheese Pizza", "Thick Crust Dough", "Plum tomato sauce", List.of("Mozzarella Cheese"));
    }
}
