package factory.method.region.chicago;

import factory.method.Pizza;

import java.util.List;

abstract class ChicagoStylePizza extends Pizza {
    ChicagoStylePizza(String name, String dough, String sauce, List<String> toppings) {
        super(name, dough, sauce, toppings);
    }

    @Override
    public void cut() {
        System.out.println("Cutting the pizza into square slices");
    }
}
