package factory.simple.cars;

import factory.simple.Car;

public class Pickup implements Car {
    private final String name;

    public Pickup() {
        this.name = "Nissan Pickup Truck";
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public void start() {
        System.out.println("Starting");
    }

    @Override
    public void drive() {
        System.out.println("Driving kinda slow");
    }

    @Override
    public void stop() {
        System.out.println("Stopping");
    }
}
