package factory.simple.cars;

import factory.simple.Car;

public class Porsche implements Car {

    private final String name;

    public Porsche() {
        this.name = "Porsche Carrera";
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public void start() {
        System.out.println("Starting");
    }

    @Override
    public void drive() {
        System.out.println("Driving really fast");
    }

    @Override
    public void stop() {
        System.out.println("Stopping");
    }
}
