package factory.simple;

public interface Car {
    String name();
    void start();
    void drive();
    void stop();
}
