package factory.simple;

import factory.simple.cars.Pickup;
import factory.simple.cars.Porsche;

import java.util.Optional;

public class CarFactory {
    public static Optional<Car> create(String carType) {
        Car car = null;
        if (carType.equals("FAST")) {
            car = new Porsche();
        } else if (carType.equals("SLOW")) {
            car = new Pickup();
        }
        return Optional.ofNullable(car);
    }
}
