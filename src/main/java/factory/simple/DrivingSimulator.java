package factory.simple;

public class DrivingSimulator {

    public void drive(String carType) {

        Car car = CarFactory.create(carType).orElseThrow();

        car.start();
        car.drive();
        car.stop();
    }

    public static void main(String[] args) {
        DrivingSimulator simulator = new DrivingSimulator();
        simulator.drive("FAST");
    }

}
