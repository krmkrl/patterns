package decorator;

public interface Beverage {
    public String description();
    public double cost();
    public Size size();

    public enum Size {
        SMALL, MEDIUM, LARGE;
    }
}


