package decorator.beverages;

import decorator.Beverage;

public class Frappuccino implements Beverage {
    private final Size size;

    public Frappuccino(Size size) {
        this.size = size;
    }

    @Override
    public String description() {
        return "Frappuccino";
    }

    @Override
    public double cost() {
        return 1.99;
    }

    @Override
    public Size size() {
        return size;
    }
}
