package decorator.beverages;

import decorator.Beverage;

public class BrewCoffee implements Beverage {
    private final Size size;

    public BrewCoffee(Size size) {
        this.size = size;
    }

    @Override
    public String description() {
        return "Brewed Coffee";
    }

    @Override
    public double cost() {
        return 1.19;
    }

    @Override
    public Size size() {
        return size;
    }
}
