package decorator.beverages;

import decorator.Beverage;

public class Tea implements Beverage {
    private final Size size;

    public Tea(Size size) {
        this.size = size;
    }
    @Override
    public String description() {
        return "Tea";
    }

    @Override
    public double cost() {
        return 0.89;
    }

    @Override
    public Size size() {
        return size;
    }
}
