package decorator.beverages;

import decorator.Beverage;

public class Decaf implements Beverage {
    private final Size size;

    public Decaf(Size size) {
        this.size = size;
    }

    @Override
    public String description() {
        return "Decaf";
    }

    @Override
    public double cost() {
        return 0.99;
    }

    @Override
    public Size size() {
        return size;
    }
}
