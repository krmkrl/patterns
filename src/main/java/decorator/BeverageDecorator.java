package decorator;

public abstract class BeverageDecorator implements Beverage {
    private final Beverage beverage;

    public BeverageDecorator(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String description() {
        return beverage.description();
    }

    @Override
    public double cost() {
        return beverage.cost();
    }

    @Override
    public Size size() {
        return beverage.size();
    }
}
