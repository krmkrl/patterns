package decorator.prizing;

import decorator.Beverage;
import decorator.BeverageDecorator;

public class FixedSizePrizing extends BeverageDecorator {
    public FixedSizePrizing(Beverage beverage) {
        super(beverage);
    }

    @Override
    public double cost() {
        double cost = super.cost();
        if (super.size() == Size.SMALL) {
            cost += 0.0;
        } else if (super.size() == Size.MEDIUM) {
            cost += 1.5;
        } else if (super.size() == Size.LARGE) {
            cost += 2.0;
        }
        return cost;
    }
}
