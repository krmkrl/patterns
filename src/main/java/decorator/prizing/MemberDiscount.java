package decorator.prizing;

import decorator.Beverage;
import decorator.BeverageDecorator;

public class MemberDiscount extends BeverageDecorator {
    public MemberDiscount(Beverage beverage) {
        super(beverage);
    }

    @Override
    public double cost() {
        return super.cost() * 0.8;
    }
}
