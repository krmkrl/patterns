package decorator;

import decorator.beverages.BrewCoffee;
import decorator.beverages.Frappuccino;
import decorator.condiments.Milk;
import decorator.condiments.Mocca;
import decorator.condiments.Sugar;
import decorator.condiments.Whip;
import decorator.prizing.FixedSizePrizing;
import decorator.prizing.MemberDiscount;

import static decorator.Beverage.Size;

public class Main {
    public static void main(String[] args) {
        Beverage coffee = new FixedSizePrizing(new Whip(new Sugar(new BrewCoffee(Size.LARGE))));
        printBeverage(coffee);

        Beverage frappeDoubleWhip = new MemberDiscount(new FixedSizePrizing(new Whip(new Whip(new Sugar(new Mocca(new Milk(new Frappuccino(Size.MEDIUM))))))));
        printBeverage(frappeDoubleWhip);
    }

    private static void printBeverage(Beverage beverage) {
        System.out.println(beverage.size() + " " + beverage.description() + " " + beverage.cost());
    }
}
