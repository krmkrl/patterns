package decorator.condiments;

import decorator.Beverage;
import decorator.BeverageDecorator;

public class Milk extends BeverageDecorator {
    public Milk(Beverage beverage) {
        super(beverage);
    }

    @Override
    public String description() {
        return super.description() + ", milk";
    }

    @Override
    public double cost() {
        return super.cost() + 0.2;
    }

}
