package decorator.condiments;

import decorator.Beverage;
import decorator.BeverageDecorator;

public class Mocca extends BeverageDecorator {
    public Mocca(Beverage beverage) {
        super(beverage);
    }

    @Override
    public String description() {
        return super.description() + ", mocca";
    }

    @Override
    public double cost() {
        return super.cost() + 0.19;
    }

}
