package decorator.condiments;

import decorator.Beverage;
import decorator.BeverageDecorator;

public class Whip extends BeverageDecorator {
    public Whip(Beverage beverage) {
        super(beverage);
    }

    @Override
    public String description() {
        return super.description() + ", whip";
    }

    @Override
    public double cost() {
        return super.cost() + 0.2;
    }
}
