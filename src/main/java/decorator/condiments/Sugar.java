package decorator.condiments;

import decorator.Beverage;
import decorator.BeverageDecorator;

public class Sugar extends BeverageDecorator {
    public Sugar(Beverage beverage) {
        super(beverage);
    }

    @Override
    public String description() {
        return super.description() + ", sugar";
    }

    @Override
    public double cost() {
        return super.cost() + 0.1;
    }
}
