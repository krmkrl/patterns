package strategy.behaviors;

@FunctionalInterface
public interface FlyBehavior {
    public void fly();

    public static FlyBehavior none() {
        return () -> {};
    }

    public static FlyBehavior normal() {
        return () -> {System.out.println("Flying normally");};
    }

    public static FlyBehavior fast() {
        return () -> {System.out.println("Flying fast");};
    }
}
