package strategy.behaviors;

@FunctionalInterface
public interface QuackBehavior {
    public void quack();

    public static QuackBehavior silent() {
        return () -> {};
    }

    public static QuackBehavior normal() {
        return () -> {System.out.println("Quack quack!");};
    }

    public static QuackBehavior talking() {
        return () -> {System.out.println("Whoopsy daisy, I can talk!");};
    }
}
