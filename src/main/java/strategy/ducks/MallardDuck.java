package strategy.ducks;

import strategy.Duck;
import strategy.behaviors.FlyBehavior;
import strategy.behaviors.QuackBehavior;

public class MallardDuck extends Duck {
    public MallardDuck() {
        super(QuackBehavior.normal(), FlyBehavior.normal());
    }

    @Override
    public void describe() {
        System.out.println("A mallard duck.");
    }
}
