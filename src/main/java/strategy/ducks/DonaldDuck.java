package strategy.ducks;

import strategy.Duck;
import strategy.behaviors.FlyBehavior;
import strategy.behaviors.QuackBehavior;

public class DonaldDuck extends Duck {
    public DonaldDuck() {
        super(QuackBehavior.talking(), FlyBehavior.none());
    }

    @Override
    public void describe() {
        System.out.println("I'm Donald Duck!");
    }
}
