package strategy.ducks;

import strategy.Duck;
import strategy.behaviors.FlyBehavior;
import strategy.behaviors.QuackBehavior;

public class BathDuck extends Duck {
    public BathDuck() {
        super(QuackBehavior.silent(), FlyBehavior.none());
    }

    @Override
    public void describe() {
        System.out.println("I'm a bathing duck");
    }
}
