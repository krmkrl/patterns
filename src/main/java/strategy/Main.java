package strategy;

import strategy.ducks.BathDuck;
import strategy.ducks.DonaldDuck;
import strategy.ducks.MallardDuck;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Duck> ducks = List.of(new MallardDuck(), new BathDuck(), new DonaldDuck());
        ducks.forEach(duck -> {
            duck.describe();
            duck.fly();
            duck.quack();
        });
    }

}
