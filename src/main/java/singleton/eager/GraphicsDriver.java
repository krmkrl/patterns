package singleton.eager;

public class GraphicsDriver {

    private static GraphicsDriver instance = new GraphicsDriver();

    public static GraphicsDriver getInstance() {
        return instance;
    }

    private GraphicsDriver() {}

    // driver related methods ...
}
