package singleton.lazy.unsafe;

public class GraphicsDriver {

    private static GraphicsDriver instance;

    public static GraphicsDriver getInstance() {
        if (instance == null) {
            instance = new GraphicsDriver();
        }
        return instance;
    }

    private GraphicsDriver() {}

    // driver related methods ...
}
