package singleton.lazy.safe.doublechecked;

public class GraphicsDriver {

    private static volatile GraphicsDriver instance;

    public static GraphicsDriver getInstance() {
        if (instance == null) {
            synchronized (GraphicsDriver.class) {
                if (instance == null) {
                    instance = new GraphicsDriver();
                }
            }
        }
        return instance;
    }

    private GraphicsDriver() {}

    // driver related methods ...
}
