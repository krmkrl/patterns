package singleton.lazy.safe.holder;

public class GraphicsDriver {

    public static GraphicsDriver getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private static class InstanceHolder {
        static final GraphicsDriver INSTANCE = new GraphicsDriver();
    }

    private GraphicsDriver() {}

    // driver related methods ...
}
