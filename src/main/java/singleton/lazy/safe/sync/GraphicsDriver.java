package singleton.lazy.safe.sync;

public class GraphicsDriver {

    private static GraphicsDriver instance;

    public synchronized static GraphicsDriver getInstance() {
        if (instance == null) {
            instance = new GraphicsDriver();
        }
        return instance;
    }

    private GraphicsDriver() {}

    // driver related methods ...
}
