package singleton.lazy.safe.enumeration;

public enum GraphicsDriver {
    INSTANCE;

    // driver related methods ...

    void blackenPixel(int xPos, int yPos) { }


    // Usage example
    public static void main(String[] args) {
        GraphicsDriver.INSTANCE.blackenPixel(23, 34);
    }

}